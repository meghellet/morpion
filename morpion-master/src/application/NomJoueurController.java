package application;



import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

/**
 *  Le Controller de la fenetre SaisieNoms qui permet la saisie des noms des deux joueurs et elle envoie �a � la fenetre jeu
  
 */
public class NomJoueurController {
	
	    @FXML
	    private TextField NomJoueur1;

	    @FXML
	    private TextField NomJoueur2;

	    @FXML
	    private Button LancerPartie;

	    @FXML
	    private Button AfficheMenu;
	    @FXML
	    private AnchorPane rootNoms;


	    @FXML
	    void AfficheMenu(ActionEvent event) {
	    	try {
	    		
	    		    Parent root = FXMLLoader.load(getClass().getResource("/application/Accueil.fxml"));
	    		    Scene scene = new Scene(root);
		            scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
		            Stage stage =new Stage();
		            stage.setResizable(false);
		            
		          stage.setTitle("Tic-Tac-Toe");
		          stage.setScene(scene);
		          stage.show();
	          
	        } catch(Exception e) {
	        	System.out.append("erreur ");
	            e.printStackTrace();
	        }
	    	
	    }

	    @FXML
	    void LancerPartie(ActionEvent event) {
	    	try {
	    		FXMLLoader loader=new FXMLLoader(getClass().getResource("/application/Jeu.fxml"));
	    		Parent root =(Parent) loader.load();
	    	    jeuController Nom = loader.getController();
	            Nom.myfunction(NomJoueur1.getText(),NomJoueur2.getText());
	            Scene scene = new Scene(root);
	            scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
	            Stage stage =new Stage();
	            stage.setResizable(false);
	            
	    		
	          stage.setTitle("Tic-Tac-Toe");
	          stage.setScene(scene);
	          stage.show();
	        
	          
	        } catch(Exception e) {
	        	System.out.append("erreur ");
	            e.printStackTrace();
	        }

	    }
	   

}
