package application;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class ReglesController {
	 @FXML
	    private AnchorPane rootRegles;

	    @FXML
	    private Button RetourMenu;

	    @FXML
	    private ImageView ImageRegles;

	    @FXML
	    void RetourMenu(ActionEvent event) {
	    	try {
	            Parent root = FXMLLoader.load(getClass().getResource("/application/Accueil.fxml"));
	            Scene scene = new Scene(root);
	            scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
	            Stage stage =new Stage();
	            stage.setResizable(false);
	            
	          stage.setTitle("Accueil");
	          stage.setScene(scene);
	          stage.show();
	        } catch(Exception e) {
	        	System.out.append("erreur ");
	            e.printStackTrace();
	        }

	    }

}
