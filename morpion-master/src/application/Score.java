package application;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *  La Classe Score permet de creer des Object scores (Nom,score) A fin d'alimenter la table score
 *   enregistrer qui se trouve dans la classe jeuController
  
 */
public class Score {

	  private  StringProperty NomTable;
	     
      private IntegerProperty ScoreTable = new SimpleIntegerProperty();
      
      public  Score(String  NomTable, Integer ScoreTable ){
          this. NomTable= new SimpleStringProperty( NomTable);
          this.ScoreTable=new SimpleIntegerProperty(ScoreTable);
      }

      public final IntegerProperty ScoreTableProperty() {
         return ScoreTable;
      }
      public final StringProperty NomTableProperty() {
          return  NomTable;
       }


      public final Integer getScore() {
         return ScoreTable.get();
      }

      public final void setScore(Integer value) {
    	  ScoreTable.set(value);
      }
      
      
    
      public StringProperty getNom() {
          return this. NomTable;
      }
      public void setNom(SimpleStringProperty nom) {
      	this. NomTable=nom;
      }
 
        
        
 
        
    
}
