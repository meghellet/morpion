package application;

import java.awt.TextArea;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.HashMap;
import java.util.ResourceBundle;
import java.util.Scanner;

import ai.MultiLayerPerceptron;
import ai.SigmoidalTransferFunction;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ToggleButton;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 *  La Classe OptionController est le controlleur de la vue MainView qui gere
 *  un espace de visualisation de la phase d�apprentissage de l�IA. visible par
 *  un indicateur de progression (ProgressBar) dans un processus d�di� (Task)
  
 */

public class OptionController implements Initializable{
	
	 

		@FXML
	    private AnchorPane rootOption;
		/////////////////////////////////////
	  
	    @FXML
	    private ChoiceBox<String> choicebox;

	    @FXML
	    private CheckBox facile;

	    @FXML
	    private CheckBox moyen;

	    @FXML
	    private CheckBox difficile;
	    
	    //////////////////////////////////////

	    @FXML
	    private Button lancer;

	    @FXML
	    private Button retour;
	    //////////////////////////////////////
	    
	    @FXML
	    private Text hText;

	    @FXML
	    private Text lrText;

	    @FXML
	    private Text nbLayersText;
	    
	    ////////////////////////////////////////////////////////
	    @FXML
	    private ProgressBar barProgress;
	    @FXML
	    private Text pourcentageText;
	    
	    ObservableList<String> list = FXCollections.observableArrayList();
	    
	    public static  MultiLayerPerceptron mlp = null;
	    
	    String oldLine = "";
	    Integer h=0;
	    Integer nbLayers=0;
	    Double lr=0.0;
	    
	    @Override
		public void initialize(URL arg0, ResourceBundle arg1) {
			
			BufferedReader reader;
			try {
				reader = new BufferedReader(new FileReader(System.getProperty("user.dir") + "/src/application/config.txt"));
				String line = reader.readLine();
				
				while (line != null) {
					if(line.contains("F")) {
						String[] facile = line.split(":");
						oldLine = line;
					}
					line = reader.readLine();
				}
				reader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
	    }
	    ////////////////////////////////////
	    //un seul checkbox cocher � la fois 
	     
	    @FXML
	    void modelDifficile(ActionEvent event) {
	    	if (difficile.isSelected()) {
	    		facile.setSelected(false);
	    		moyen.setSelected(false);
	    	}
	    	

	   }

	    @FXML
	    void modelFacile(ActionEvent event) {
	    	if (facile.isSelected()) {
	    		difficile.setSelected(false);
	    		moyen.setSelected(false);
	    	}

	    }

	    @FXML
	    void modelMoyen(ActionEvent event) {
	    	if (moyen.isSelected()) {
	    		facile.setSelected(false);
	    		difficile.setSelected(false);
	    	}
	     }
	    
		
	  
	    @FXML
	    void LancerLaPartie(ActionEvent event) throws Exception {
	    	saveConfig(event);
	    }
	    ////////////////////////////////////////
	    //LANCER L'APPRENTISSAGE DANS UN THREAD SEPARER POUR EVITER QUE L'INTERFACE UTILISATEUR SOIT FIGER 
	    //LANCER LA FENETRE JEU -->COMMENCER LA PARTIE

	
	    public void loadDifficultyData() {
			 
			 String value = "";
			 if(facile.isSelected()) {
				 value = "F";
				 System.out.append("je suis facile ");
				
			 }
			 else if(moyen.isSelected()) {
				 value = "M";
				 System.out.append("je suis moyen");
			 }
			 else if(difficile.isSelected()) {
				 value = "D";
				 System.out.append("je suis difficile");
			 }
			 
			 
			 BufferedReader reader;
				try {
					reader = new BufferedReader(new FileReader(System.getProperty("user.dir") + "/src/application/config.txt"));
					String line = reader.readLine();
					
					while (line != null) {
						
						if(line.contains(value)) {
							String[] lineSplit = line.split(":");
							//recuperer ls params
						    h=Integer.parseInt(lineSplit[1]);
							hText.setText(String.valueOf(h));
						    
						   
						    lr=Double.parseDouble(lineSplit[2]);
						    lrText.setText(String.valueOf(lr));
						   
						 
						    nbLayers=Integer.parseInt(lineSplit[3]);
						    System.out.append("la valeur de nbLayers"+nbLayers);
						    nbLayersText.setText(String.valueOf(nbLayers));
						    oldLine = line;
						}
						line = reader.readLine();
					}
					reader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		 }
	    
	    
	    /////////////////////////////
	    //lancer l'apprentissage
	    boolean saveConfig(ActionEvent event) throws IOException {
	    	
			  String filePath = System.getProperty("user.dir") + "/src/application/config.txt";
		      Scanner sc = new Scanner(new File(filePath));
		      StringBuffer buffer = new StringBuffer();
		      while (sc.hasNextLine()) {
		         buffer.append(sc.nextLine()+System.lineSeparator());
		      }
		      String fileContents = buffer.toString();
		      sc.close();

		      String newLine = "";
				 if(facile.isSelected()) {
					 newLine = "F:"+ Integer.valueOf(hText.getText()) +":"+Double.valueOf(lrText.getText())+":"+  Integer.valueOf(nbLayersText.getText());
				 }
				 else if(moyen.isSelected()) {
					 newLine = "M:"+Integer.valueOf(hText.getText())+":"+Double.valueOf(lrText.getText())+":"+  Integer.valueOf(nbLayersText.getText());
				 }
				 else if(difficile.isSelected()) {
					 newLine = "D:"+Integer.valueOf(hText.getText())+":"+Double.valueOf(lrText.getText())+":"+  Integer.valueOf(nbLayersText.getText());
				 }
				 System.out.println("oldLine : " + oldLine);
				 System.out.println("newLine : " + newLine);

		      fileContents = fileContents.replaceAll(oldLine, newLine);
		      
		      FileWriter writer = new FileWriter(filePath);
		      writer.append(fileContents);
		      writer.flush();
		      
		      
		      String fileSrl = System.getProperty("user.dir") + "/src/application/Models/mlp_" +Integer.valueOf(hText.getText())+ "_" + Double.valueOf(lrText.getText()) + "_" +Integer.valueOf(nbLayersText.getText())+ ".srl";
		      int h = Integer.valueOf(hText.getText());
		      double lr = Double.valueOf(lrText.getText()) ;
		      int l = Integer.valueOf(nbLayersText.getText());

		      System.out.println(fileSrl);
		      File tmpFile = new File(fileSrl);
		      boolean exists = tmpFile.exists();
		      if(exists == false) {
//////////////////////////////////////////////////////////////////////////////////
		    	  //On lance le task
				  Task<Void> task = new Task<Void>() {
					  @Override protected Void call() {
						  System.out.println("h : " + h);
						  System.out.println("lr : " + lr);
						  System.out.println("nbrLayers : " + l);


						  int[] layers = new int[l + 2];
						  layers[0] = 9;
						  for (int i = 0; i < l; i++) {
							  layers[i + 1] = h;
						  }
						  layers[layers.length - 1] = 9;


						  for (int i = 0; i < layers.length; i++) {
							  System.out.println(layers[i]);
						  }

						  try {

							  System.out.println();
							  System.out.println("START TRAINING ...");
							  System.out.println();

							  double error = 0.0;
							  MultiLayerPerceptron net = new MultiLayerPerceptron(layers, lr, new SigmoidalTransferFunction());
							  //				double epochs = 1000000 ;
							  double epochs = 100000;

							  System.out.println("---");
							  System.out.println("Load data ...");
							  HashMap<Integer, Coup> mapTrain = loadCoupsFromFile("./resources/train_dev_test/train.txt");
							  HashMap<Integer, Coup> mapDev = loadCoupsFromFile("./resources/train_dev_test/dev.txt");
							  HashMap<Integer, Coup> mapTest = loadCoupsFromFile("./resources/train_dev_test/test.txt");
							  System.out.println("---");
							  //TRAINING ...
							  int u = 0;
							  for (int i = 0; i < epochs; i++) {

								  Coup c = null;
								  while (c == null)
									  c = mapTrain.get((int) (Math.round(Math.random() * mapTrain.size())));

								  error += net.backPropagate(c.in, c.out);
								  float pourcentage = 0;
								  if (i % 100000 == 0) {
									  System.out.println("Error at step " + i + " is " + (error / (double) i));
									  u = i;
									  
								  }
								  pourcentage = (float) ((i / epochs) * 100);
								  updateMessage(String.valueOf(pourcentage));
								  updateProgress(i, epochs);
							  }


							  barProgress.setProgress(1);
							  error /= epochs;
							  if (epochs > 0)
								  System.out.println("Error is " + error);
							  //
							  System.out.println("Learning completed!");
							  pourcentageText.setText("Learning completed!");
							  net.save(fileSrl);
							  updateMessage("saved");

							  //TEST ...

						  } catch (Exception e) {
							  System.out.println("Test.test()");
							  e.printStackTrace();
							  System.exit(-1);
						  }
						  return null;
					  }
				  };

				  Thread t = new Thread(task);

				  t.start();

				  task.messageProperty().addListener(new ChangeListener<String>() {
					  @Override
					  public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {

					  	if(newValue.equals("saved")){
					  		//recuperer le fichier qu'on a save 
					  		mlp = ai.MultiLayerPerceptron.load(fileSrl);
					  		
					  		try {
					            Parent root = FXMLLoader.load(getClass().getResource("/application/JeuIA.fxml"));
					            Scene scene = new Scene(root);
					            scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
					            Stage stage =new Stage();
					            stage.setResizable(false);
					            
					          stage.setTitle("Tic-Tac-Tae");
					          stage.setScene(scene);
					          stage.show();
					        } catch(Exception e) {
					        	System.out.append("erreur ");
					            e.printStackTrace();
					        }
							 
						}
//					  	else{
							pourcentageText.setText(newValue + " %");
//						}
					  }
				  });

				  task.progressProperty().addListener(new ChangeListener<Number>() {
					  @Override
					  public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
						  barProgress.setProgress((double) newValue);
					  }

				  });



		      }
		      return false;
		 }
	    //////////////////////////////////////////////
	    
	    
	    public static HashMap<Integer, Coup> loadCoupsFromFile(String file){
			System.out.println("loadCoupsFromFile from "+file+" ...");
			HashMap<Integer, Coup> map = new HashMap<>();
			try {
				BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(new File(file))));
				String s = "";
				while ((s = br.readLine()) != null) {
					String[] sIn = s.split("\t")[0].split(" ");
					String[] sOut = s.split("\t")[1].split(" ");

					double[] in = new double[sIn.length];
					double[] out = new double[sOut.length];

					for (int i = 0; i < sIn.length; i++) {
						in[i] = new Double(sIn[i]);
					}

					for (int i = 0; i < sOut.length; i++) {
						out[i] = new Double(sOut[i]);
					}

					Coup c = new Coup(9, "");
					c.in = in ;
					c.out = out ;
					map.put(map.size(), c);
				}
				br.close();
			}
			catch (Exception e) {
				System.out.println("Test.loadCoupsFromFile()");
				e.printStackTrace();
				System.exit(-1);
			}
			return map ;
		}
	    
	    
	    
	 
	 
	 
	 /////////////////////////////////////////
	 //Retour � l'accueil

	    @FXML
	    void RetourMenu(ActionEvent event) {
	    	try {
	            Parent root = FXMLLoader.load(getClass().getResource("/application/Accueil.fxml"));
	            Scene scene = new Scene(root);
	            scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
	            Stage stage =new Stage();
	            stage.setResizable(false);
	            
	          stage.setTitle("Tic-Tac-Tae");
	          stage.setScene(scene);
	          stage.show();
	        } catch(Exception e) {
	        	System.out.append("erreur ");
	            e.printStackTrace();
	        }

	    }
	    /////////////////////////////////////////
	    //Aller Voir les modeles deja charger
	    
	    @FXML
	    void VoirModeles(ActionEvent event) {
	    	try {
	            Parent root = FXMLLoader.load(getClass().getResource("/application/ModelesIA.fxml"));
	            Scene scene = new Scene(root);
	            scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
	            Stage stage =new Stage();
	            stage.setResizable(false);
	            
	          stage.setTitle("Tic-Tac-Tae");
	          stage.setScene(scene);
	          stage.show();
	        } catch(Exception e) {
	        	System.out.append("erreur ");
	            e.printStackTrace();
	        }

	    }

}
