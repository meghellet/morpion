package application;

import java.net.URL;

import java.util.ResourceBundle;

import javafx.animation.TranslateTransition;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.util.Duration;
/**
 *  La Classe JeuIAController est le controller de l'interface de jeu ,il permet de gerer l'IA du jeu 

 */

public class JeuIAController  implements Initializable{
	
	
	////////////////////////////////////////////
	
	@FXML
    private AnchorPane rootJeuIA;
	@FXML
	private Label labelTransition;
   
   //////////////////////////////////////////////
    @FXML
    private Button retour;

    @FXML
    private Button buttonQuitter;
    
    //////////////////////////////////

    @FXML
    private TableView<Score> Table;

    @FXML
    private TableColumn<Score,  StringProperty> NomTable;

    @FXML
    private TableColumn<Score, IntegerProperty> ScoreTable;
    
    /////////////////////////////////////

    @FXML
    private ImageView ImageView1;

    @FXML
    private ImageView ImageView2;

    @FXML
    private ImageView ImageView3;

    @FXML
    private ImageView ImageView4;

    @FXML
    private ImageView ImageView5;

    @FXML
    private ImageView ImageView6;

    @FXML
    private ImageView ImageView7;

    @FXML
    private ImageView ImageView8;

    @FXML
    private ImageView ImageView9;
    //////////////////////////////////

    @FXML
    private Label setNomJoueur1;

    @FXML
    private Label setNomJoueur2;

    @FXML
    private Label gagnant;

    @FXML
    private Label TourJoueur;

    @FXML
    private Label score1;

    @FXML
    private Label score2;
    ////////////////////////////////////
    @FXML
    private Button Recommencer;

    @FXML
    private Button enregistrer;
    
    /////////////////////////

    @FXML
    private ImageView ImageJoueur1;

    @FXML
    private ImageView ImageJoueur2;

    @FXML
    private ImageView ImageVs;
    
    //////////////////////////
    //Charger les Images dans les itemsviews
    Image ButtonPressed=new Image("images/rondRouge.png");
    Image ButtonReleased = new Image("images/croixverte.png");
    Image ImageWin = new Image("images/win.png");
    ////////////////////////////
    
    
    int []cases=new int[9];
	int NbrClic=0;
	int scoreJoueur1 = 0;
	int scoreJoueur2 = 0;
	public static ai.MultiLayerPerceptron net;
	public static double[] inputs;
	public static double[] output;
	//////////////////////////////////////////
	// permet de recuperer les informations du  modele d'apprentissage de l'IA
	void setnet() {
		this.net= OptionController.mlp;
	}
	

    /*
     *les MoussePresssed sont des actions qui quand on click sur les case du morpion on affiche le coup du joeur.
     *Puis on testes si le jouer a gagner sinon l'ia joue.
     */
    @FXML
    void MousePressed(MouseEvent event) {
    	cases[0]=1;
    	ImageView1.setImage(ButtonPressed);
    	ImageView1.setDisable(true);
    	System.out.append("le button rond est place!!!!");
    	String Joueur1= setNomJoueur2.getText();
    	if(!Joueur1.equals("")) {
	    	TourJoueur.setText("C'est Votre Tour "+Joueur1);
	    	TourJoueur.setStyle("-fx-text-fill: white; -fx-font-size: 14px;");
	    	}
    	for (int i=0;i<cases.length;i++) {
    		System.out.print(cases[i]);
    	}
    	System.out.println();
    	System.out.println("--------next----------");
    	boolean gagant=TestGagnant(cases,1);
    	if (gagant==false) {
    		TourIA();
    		TestGagnant(cases,2);
    	}

    }

    @FXML
    void MousePressed2(MouseEvent event) {
    	cases[1]=1;
    	ImageView2.setImage(ButtonPressed);
    	ImageView2.setDisable(true);
    	System.out.append("le button rond est place!!!!");
    	String Joueur1= setNomJoueur2.getText();
    	if(!Joueur1.equals("")) {
	    	TourJoueur.setText("C'est Votre Tour "+Joueur1);
	    	TourJoueur.setStyle("-fx-text-fill: white; -fx-font-size: 14px;");
	    	}
    	for (int i=0;i<cases.length;i++) {
    		System.out.print(cases[i]);
    	}
    	System.out.println();
    	System.out.println("--------next----------");
    	boolean gagant=TestGagnant(cases,1);
    	if (gagant==false) {
    		TourIA();
    		TestGagnant(cases,2);
    	}

    }

    @FXML
    void MousePressed3(MouseEvent event) {
    	cases[2]=1;
    	ImageView3.setImage(ButtonPressed);
    	ImageView3.setDisable(true);
    	System.out.append("le button rond est place!!!!");
    	String Joueur1= setNomJoueur2.getText();
    	if(!Joueur1.equals("")) {
	    	TourJoueur.setText("C'est Votre Tour "+Joueur1);
	    	TourJoueur.setStyle("-fx-text-fill: white; -fx-font-size: 14px;");
	    	}
    	for (int i=0;i<cases.length;i++) {
    		System.out.print(cases[i]);
    	}
    	System.out.println();
    	System.out.println("--------next----------");
    	boolean gagant=TestGagnant(cases,1);
    	if (gagant==false) {
    		TourIA();
    		TestGagnant(cases,2);
    	}
  

    }

    @FXML
    void MousePressed4(MouseEvent event) {
    	cases[3]=1;
    	ImageView4.setImage(ButtonPressed);
    	ImageView4.setDisable(true);
    	System.out.append("le button rond est place!!!!");
    	String Joueur1= setNomJoueur2.getText();
    	if(!Joueur1.equals("")) {
	    	TourJoueur.setText("C'est Votre Tour "+Joueur1);
	    	TourJoueur.setStyle("-fx-text-fill: white; -fx-font-size: 14px;");
	    	}
    	for (int i=0;i<cases.length;i++) {
    		System.out.print(cases[i]);
    	}
    	System.out.println();
    	System.out.println("--------next----------");
    	boolean gagant=TestGagnant(cases,1);
    	if (gagant==false) {
    		TourIA();
    		TestGagnant(cases,2);
    	}
    }

    @FXML
    void MousePressed5(MouseEvent event) {
    	cases[4]=1;
    	ImageView5.setImage(ButtonPressed);
    	ImageView5.setDisable(true);
    	System.out.append("le button rond est place!!!!");
    	String Joueur1= setNomJoueur2.getText();
    	if(!Joueur1.equals("")) {
	    	TourJoueur.setText("C'est Votre Tour "+Joueur1);
	    	TourJoueur.setStyle("-fx-text-fill: white; -fx-font-size: 14px;");
	    	}
    	for (int i=0;i<cases.length;i++) {
    		System.out.print(cases[i]);
    	}
    	System.out.println();
    	System.out.println("--------next----------");
    	boolean gagant=TestGagnant(cases,1);
    	if (gagant==false) {
    		TourIA();
    		TestGagnant(cases,2);
    	}

    }

    @FXML
    void MousePressed6(MouseEvent event) {
    	cases[5]=1;
    	ImageView6.setImage(ButtonPressed);
    	ImageView6.setDisable(true);
    	System.out.append("le button rond est place!!!!");
    	String Joueur1= setNomJoueur2.getText();
    	if(!Joueur1.equals("")) {
	    	TourJoueur.setText("C'est Votre Tour "+Joueur1);
	    	TourJoueur.setStyle("-fx-text-fill: white; -fx-font-size: 14px;");
	    	}
    	for (int i=0;i<cases.length;i++) {
    		System.out.print(cases[i]);
    	}
    	System.out.println();
    	System.out.println("--------next----------");
    	boolean gagant=TestGagnant(cases,1);
    	if (gagant==false) {
    		TourIA();
    		TestGagnant(cases,2);
    	}
    }

    @FXML
    void MousePressed7(MouseEvent event) {
    	cases[6]=1;
    	ImageView7.setImage(ButtonPressed);
    	ImageView7.setDisable(true);
    	System.out.append("le button rond est place!!!!");
    	String Joueur1= setNomJoueur2.getText();
    	if(!Joueur1.equals("")) {
	    	TourJoueur.setText("C'est Votre Tour "+Joueur1);
	    	TourJoueur.setStyle("-fx-text-fill: white; -fx-font-size: 14px;");
	    	}
    	for (int i=0;i<cases.length;i++) {
    		System.out.print(cases[i]);
    	}
    	System.out.println();
    	System.out.println("--------next----------");
    	boolean gagant=TestGagnant(cases,1);
    	if (gagant==false) {
    		TourIA();
    		TestGagnant(cases,2);
    	}
    }

    @FXML
    void MousePressed8(MouseEvent event) {
    	cases[7]=1;
    	ImageView8.setImage(ButtonPressed);
    	ImageView8.setDisable(true);
    	System.out.append("le button rond est place!!!!");
    	String Joueur1= setNomJoueur2.getText();
    	if(!Joueur1.equals("")) {
	    	TourJoueur.setText("C'est Votre Tour "+Joueur1);
	    	TourJoueur.setStyle("-fx-text-fill: white; -fx-font-size: 14px;");
	    	}
    	for (int i=0;i<cases.length;i++) {
    		System.out.print(cases[i]);
    	}
    	System.out.println();
    	System.out.println("--------next----------");
    	boolean gagant=TestGagnant(cases,1);
    	if (gagant==false) {
    		TourIA();
    		TestGagnant(cases,2);
    	}
    }

    @FXML
    void MousePressed9(MouseEvent event) {
    	cases[8]=1;
    	ImageView9.setImage(ButtonPressed);
    	ImageView9.setDisable(true);
    	System.out.append("le button rond est place!!!!");
    	String Joueur1= setNomJoueur2.getText();
    	if(!Joueur1.equals("")) {
	    	TourJoueur.setText("C'est Votre Tour "+Joueur1);
	    	TourJoueur.setStyle("-fx-text-fill: white; -fx-font-size: 14px;");
	    	}
    	for (int i=0;i<cases.length;i++) {
    		System.out.print(cases[i]);
    	}
    	System.out.println();
    	System.out.println("--------next----------");
    	boolean gagant=TestGagnant(cases,1);
    	if (gagant==false) {
    		TourIA();
    		TestGagnant(cases,2);
    	}

    }

  
    
    /////////////////////////////////////////////////
    //Entregistrer les Scores des joueurs en ajoutant les objects score dans la table score enregistrés
    
    

    @FXML
    void EnregistrerResultat(ActionEvent event) {
    	
    	NomTable.setCellValueFactory(new PropertyValueFactory<Score,  StringProperty>("NomTable"));
        ScoreTable.setCellValueFactory(new PropertyValueFactory<Score, IntegerProperty>("ScoreTable"));
        final ObservableList<Score> data = FXCollections.observableArrayList();
    	data.add(new Score("essaid",10));
    	data.add(new Score("Pedro",15));
    	data.add(new Score("Pepe",60));
    	data.add(new Score(setNomJoueur1.getText(),Integer.valueOf(score1.getText())));
    	data.add(new Score(setNomJoueur2.getText(),Integer.valueOf(score2.getText())));
    	
       Table.getItems().addAll(data);

    }
    //////////////////////////////////////////

    @FXML
    void RelancerPartie(ActionEvent event) {
    	//rendre le ImageView non cliquable
		ImageView1.setDisable(false);
		ImageView2.setDisable(false);
		ImageView3.setDisable(false);
		ImageView4.setDisable(false);
		ImageView5.setDisable(false);
		ImageView6.setDisable(false);
		ImageView7.setDisable(false);
		ImageView8.setDisable(false);
		ImageView9.setDisable(false);
	
	// enlever les Images des imageView
		ImageView1.setImage(null);
		ImageView2.setImage(null);
		ImageView3.setImage(null);
		ImageView4.setImage(null);
		ImageView4.setImage(null);
		ImageView5.setImage(null);
		ImageView6.setImage(null);
		ImageView7.setImage(null);
		ImageView8.setImage(null);
		ImageView9.setImage(null);
		NbrClic = 0 ; //remise du compteur de Clic A zero pour une nouvelle partie 
		for(int i=0;i<9;i++) { //remise du tableau de cases a zero 
			cases[i]=0;
		}
		gagnant.setText(null);
		TourJoueur.setText(null);
		

    }
    
    ///////////////////////////////

    @FXML
    void UploadView(ActionEvent event) {
    	try {
            Parent root = FXMLLoader.load(getClass().getResource("/application/Accueil.fxml"));
            Scene scene = new Scene(root);
            scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
            Stage stage =new Stage();
            stage.setResizable(false);
            
          stage.setTitle("Accueil");
          stage.setScene(scene);
          stage.show();
        } catch(Exception e) {
        	System.out.append("erreur ");
            e.printStackTrace();
        }


    }
    
    //////////////////////////////////////
    
    @FXML
    void Quitter(ActionEvent event) {
    	Stage stage = (Stage) buttonQuitter.getScene().getWindow();
	     stage.close();

    }
    
    /////////////////////////////////////////////
    
    public boolean GagnantHorizontal(int cases[],int joueur) {
    	
        if(cases[0] == joueur && cases[1] ==joueur && cases[2] == joueur  ) {
        	ImageView1.setImage(ImageWin);
        	ImageView2.setImage(ImageWin);
        	ImageView3.setImage(ImageWin);
    		return true;
    	}
        if(cases[3] == joueur && cases[4] ==joueur && cases[5] == joueur  ) {
        	ImageView4.setImage(ImageWin);
        	ImageView5.setImage(ImageWin);
        	ImageView6.setImage(ImageWin);
    		return true;
    	}
        if(cases[6] == joueur && cases[7] ==joueur && cases[8] == joueur  ) {
        	ImageView7.setImage(ImageWin);
        	ImageView8.setImage(ImageWin);
        	ImageView9.setImage(ImageWin);
    		return true;
    	}
        else {
        return false ;
        }
    }
    
    ///////////////////////////////////////
    
    public boolean GagnantVertical (int cases[],int joueur) {
    	
        if(cases[0] == joueur && cases[3] ==joueur && cases[6] == joueur  ) {
        	ImageView1.setImage(ImageWin);
        	ImageView4.setImage(ImageWin);
        	ImageView7.setImage(ImageWin);
    		return true;
    	}
        if(cases[1] == joueur && cases[4] ==joueur && cases[7] == joueur  ) {
        	ImageView2.setImage(ImageWin);
        	ImageView5.setImage(ImageWin);
        	ImageView8.setImage(ImageWin);
    		return true;
    	}
        if(cases[2] == joueur && cases[5] ==joueur && cases[8] == joueur  ) {
        	ImageView3.setImage(ImageWin);
        	ImageView6.setImage(ImageWin);
        	ImageView9.setImage(ImageWin);
    		return true;
    	}
        
        else {
        return false ;
        }
    }
    
    ////////////////////////////////////////////
    //Un fonction qui gere le tour de IA selon ls resultats renvoyer dans le tableau output
    public void TourIA() {
    	inputs = new double[] {0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0};
    	System.out.println(net);
    	setnet();
    	//cree le tableu des probabilite
    	output = net.forwardPropagation(inputs);
		
		
		System.out.println("****output******");
		//trie les cases en fonction des probabilite tableau[0] est egale a la casse qui a la plus grande probabilites
		int tableau[] = tableauRetournIndice();
		
		System.out.println("-----------------");
		
		int i=0;
		boolean trouve = false;
		String s ="";
		//le while permet de jouer le coup de l'ia dans la casse qui a la plus grande probabiliter si elle n'est pas jouer
		while(trouve == false && i<9) {

			if(((tableau[i]) == 0) && (cases[0]==0)) {
				ImageView1.setImage(ButtonReleased);
				ImageView1.setDisable(true);
				cases[0]=2;
    			trouve=true;
            }else if(((tableau[i]) == 1) && (cases[1]==0)) {
            	ImageView2.setImage(ButtonReleased);
            	ImageView2.setDisable(true);
				cases[1]=2;
            	trouve=true;
            }else if(((tableau[i]) == 2) && (cases[2]==0)) {
            	ImageView3.setImage(ButtonReleased);
            	ImageView3.setDisable(true);
				cases[2]=2;
            	trouve=true;
            }else if(((tableau[i]) == 3) && (cases[3]==0)) {
            	ImageView4.setImage(ButtonReleased);
            	ImageView4.setDisable(true);
				cases[3]=2;
            	trouve=true;
            }else if(((tableau[i]) == 4) && (cases[4]==0)) {
            	ImageView5.setImage(ButtonReleased);
            	ImageView5.setDisable(true);
				cases[4]=2;
            	trouve=true;
            }else if(((tableau[i]) == 5) && (cases[5]==0)) {
            	ImageView6.setImage(ButtonReleased);
            	ImageView6.setDisable(true);
				cases[5]=2;
            	trouve=true;
           	}else if(((tableau[i]) == 6) && (cases[6]==0)) {
           		ImageView7.setImage(ButtonReleased);
           		ImageView7.setDisable(true);
				cases[6]=2;
            	trouve=true;
           	}else if(((tableau[i]) == 7) && (cases[7]==0)) {
           		ImageView8.setImage(ButtonReleased);
           		ImageView8.setDisable(true);
				cases[7]=2;
           		trouve=true;
           	}else if(((tableau[i]) == 8) && (cases[8]==0)) {
           		ImageView9.setImage(ButtonReleased);
           		ImageView9.setDisable(true);
				cases[8]=2;
           		trouve=true;
           	}
			i++;
		}
		
	}

    
    
    
    /////////////////////////////////////////////
    //TEST GAGNANT POUR LES CASES EN DIAGONALS
    //AFFICHER L'IMAGE GAGNANT DANS LES CASES CORRESPONDANTES 
    
    public boolean GagnantDiagonal (int cases[],int joueur) {
    	
        if(cases[0] == joueur && cases[4] ==joueur && cases[8] == joueur  ) {
        	ImageView1.setImage(ImageWin);
        	ImageView5.setImage(ImageWin);
        	ImageView9.setImage(ImageWin);
    		return true;
    	}
        if(cases[2] == joueur && cases[4] ==joueur && cases[6] == joueur  ) {
        	ImageView3.setImage(ImageWin);
        	ImageView5.setImage(ImageWin);
        	ImageView7.setImage(ImageWin);
    		return true;
    	}
       
       else {
        return false ;
        }
    }
    
    ////////////////////////////////////////////////////////
    /**
     *  TestGagnant une fonction qui permet de verifier si il existe un gagnant ou pas 
     *  si vrai ---> le reste des cases ne sont plus cliquable -->on incrimente le score du joueur gagnant -->fin partie 
     *  @param cases : un tableau avec des cases jouer par le joueur 
     *  @param joueur : la personne qui a jouer le coup 
     *  @return boolean :pour savoir si il ya un gagant ou pas

     */

     public boolean TestGagnant(int cases[],int joueur) {
    	
    	if(GagnantDiagonal(cases, joueur) || GagnantHorizontal(cases, joueur)|| GagnantVertical(cases, joueur) ) {
    		ImageView1.setDisable(true);
			ImageView2.setDisable(true);
			ImageView3.setDisable(true);
			ImageView4.setDisable(true);
			ImageView5.setDisable(true);
			ImageView6.setDisable(true);
			ImageView7.setDisable(true);
			ImageView8.setDisable(true);
			ImageView9.setDisable(true);
			
			if(joueur == 1) { //si le joueur 1 est gagnant
        					    System.out.println(" on a un gagnant ");
        						gagnant.setText("Felicitation! Le joueur 1 est Gagnant ");
        					    gagnant.setStyle("-fx-text-fill: white; -fx-font-size: 15px;");
        						
        					    scoreJoueur1 +=1;
        						System.out.println("le resultat du joueur 1 est  "+scoreJoueur1);
        						score1.setText(String.valueOf(scoreJoueur1));
        						
        						
        	}
        	else if(joueur == 2) {
        		System.out.println(" on a un gagnant ");
				gagnant.setText("Felicitation! Le joueur 2 est Gagnant");
				gagnant.setStyle("-fx-text-fill: white; -fx-font-size: 15px;");
				
				scoreJoueur2 +=1;
				System.out.println("le resultat du joueur 2 est  "+scoreJoueur2);
				score2.setText(String.valueOf(scoreJoueur2));
        	}
			return true;
        		
        }
        		
       else {
    	   		System.out.println(" pas de gagnant ");
        		gagnant.setText("A vous de jouer! ");
        	}
  
        	for(int i=0;i<cases.length;i++) {
        		System.out.println("la valeur"+i+" de la cases :"+cases[i]);
        	}
        	return false;
        	
    	
   }
     
     ////////////////////////////////////////////////////////////////
 	//cette fonction renvoie l'indice le plus grand d'un tableau
 	public int indicePlusGrand(double[] contenu) {
 		int indice=0;
 		double max=contenu[0];
 		for (int i=1; i<contenu.length; i++) {
 		    if (max < contenu[i]) {
 			max=contenu[i];
 			indice=i;
 		    }
 		}
 		return indice;
 	}
 	public int[]  tableauRetournIndice() {
		int[] tableau = new int[output.length];
		for(int i=0;i<tableau.length; i++) {
			tableau[i] = 0;
		}
		
		for (int i = 0; i < output.length; i++) {
			int max=indicePlusGrand(output);
			tableau[i]=max;
			output[max]=0;
		}
		return tableau;
	}
 	
 	
 	///////////////////////////////////////////////////////////////////
 	//Gerer la Transition du message benvenue au lancement de la fenetre 


	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub
		TranslateTransition Translate = new TranslateTransition(Duration.seconds(5), labelTransition);
		Translate.setToX(labelTransition.getLayoutX() +rootJeuIA.getPrefWidth());
		Translate.play();
		
	}



}
