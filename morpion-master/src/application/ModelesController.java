package application;

import java.awt.Choice;

import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;



/**
 *  La Classe ModelesController est le controller de l-interface qui permet d'afficher les modeles pour lesquels on a deja fait l'apprentissage 
 *  comme on peut supprimer un model selectionné dans le choicebox 

 */

public class ModelesController implements Initializable {
	    @FXML
	    private javafx.scene.control.ChoiceBox<String> ChoiceBox;

	    @FXML
	    private Button supprimer;

	    @FXML
	    private Button Retour;
	    
	   int file=0;
	    
	    ObservableList<String> list = FXCollections.observableArrayList();
	    
	    
	    
	    @Override
		 public void initialize(URL url, ResourceBundle rb) {
			  ChargeDonnee();
			  ChoiceBox.getSelectionModel().selectedIndexProperty().addListener(
				         (ObservableValue<? extends Number> ov, Number old_val, Number new_val) -> {
				            file=(int) new_val;
				      });
		 }
	    ////////////////////////////////////////
	    //charger les models dans le dossier Models

		 
	    private void ChargeDonnee() {
	    	String currentDirectory = System.getProperty("user.dir");
	    	System.out.println(currentDirectory);
	    	final File folder = new File(currentDirectory + "/src/application/Models");
	    	AjoutFichierDansDossier(folder);
	    }
	    /////////////////////////////////////
	    //Ajout ds nouveaux models dans le choiceBox
		 
	   public void AjoutFichierDansDossier(final File folder) {
		   list.removeAll(list);

		    for (final File fileEntry : folder.listFiles()) {
		        if (fileEntry.isDirectory()) {
		            AjoutFichierDansDossier(fileEntry);
		        } else {
		            System.out.println(fileEntry.getName());
		   		 	list.addAll(fileEntry.getName());
		        }
		        
		    }
		    ChoiceBox.getItems().addAll(list);
		}
	   
	   public void listFilesForId(final File folder, int id) {
		  int cpt = 0;
		    for (final File fileEntry : folder.listFiles()) {
		    	System.out.println(id);
		    	System.out.println(cpt);
		        if(id == cpt) {
		        	SupprimeModeleFichier(fileEntry);
		        }
		        cpt++;
		    }
		}
	   
	   	public void SupprimeModeleFichier(final File file) {
	   		if(file.delete()) {
	   			System.out.println(file.getName() + " supprimer");
	   		}
	   		else {
	   			System.out.println("erreur de suppression du fichier : " + file.getName());  
	   		}
	   	}
	   	
	   	
		 
		 /////////////////////////////////////////
		 //retour a l'interface d'accueil

	    @FXML
	    void RetourMenu(ActionEvent event) {
	    	try {
	            Parent root = FXMLLoader.load(getClass().getResource("/application/Accueil.fxml"));
	            Scene scene = new Scene(root);
	            scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
	            Stage stage =new Stage();
	            stage.setResizable(false);
	            
	          stage.setTitle("Tic-Tac-Tae");
	          stage.setScene(scene);
	          stage.show();
	        } catch(Exception e) {
	        	System.out.append("erreur ");
	            e.printStackTrace();
	        }

	    

	    }
	    
	    
	    ////////////////////////////////////
	    //Fonction qui permet d supprimr un modele dans la list en faisant appel a la fonction remove 

	    @FXML
	    void SupprimerModele(ActionEvent event) {
	    	String currentDirectory = System.getProperty("user.dir");
		   	System.out.println(currentDirectory);
			final File folder = new File(currentDirectory + "/src/application/Models");
			
			String fileName = "mlp_128_0.2_5.srl";
			
			File fileToDelete = new File(currentDirectory + "/src/application/Models/" + fileName);
			listFilesForId(folder, file);
		 	System.out.println("L'indice du fichier depuis la liste : " + file);
            list.remove(file);
		 	ChoiceBox.getItems().clear();
		 	ChoiceBox.getItems().addAll(list);

	    }


}
