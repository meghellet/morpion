package application;

import java.awt.event.ActionEvent;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.animation.RotateTransition;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;
import javafx.util.Duration;



/**
 *  La Classe AccuielController est le controlleur associer au fichir fxml Accuiel 
 *  permet de gerer l'affichage de la fenetre d'accuiel 

 */


public class AccueilController implements Initializable {
	    @FXML
	    private AnchorPane root;
	    @FXML
	    private Button buttonQuitter;
	    @FXML
	    private Button buttonSolo;
	    @FXML
	    private Button buttonMultiJoueur;
	    
	    @FXML
	    private Button Regles;
	    
	    @FXML
	    private Circle c1;

	    @FXML
	    private Circle c2;

	    @FXML
	    private Circle c3;
	   
	    
	    
	    
	    ///////////////////////////////////////
	    //Quitter L'interface du jeu

	    @FXML
	    void Quitter(javafx.event.ActionEvent event) {
	    	 Stage stage = (Stage) buttonQuitter.getScene().getWindow();
		     stage.close();

	    }
	    
	    //////////////////////////////////////////
	    //charger la fenetre Options quand on clique sur le button 
	    @FXML
	    void getOption(javafx.event.ActionEvent event) {
	    	try {
	            Parent root = FXMLLoader.load(getClass().getResource("/application/Options.fxml"));
	            Scene scene = new Scene(root);
	            scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
	            Stage stage =new Stage();
	            stage.setResizable(false);
	            
	          stage.setTitle("Tic-Tac-Toe");
	          stage.setScene(scene);
	          stage.show();
	        } catch(Exception e) {
	        	System.out.append("erreur ");
	            e.printStackTrace();
	        }

	    }
	    
	    //////////////////////////////////////
	    //charger la fenetre SaisieNoms
	    @FXML
	    void getJeu(javafx.event.ActionEvent event) {
	    	try {
	            Parent root = FXMLLoader.load(getClass().getResource("/application/SaisieNoms.fxml"));
	            Scene scene = new Scene(root);
	            scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
	            Stage stage =new Stage();
	            stage.setResizable(false);
	           
	            
	          stage.setTitle("Tic-Tac-Toe");
	          stage.setScene(scene);
	          stage.show();
	        } catch(Exception e) {
	        	System.out.append("erreur ");
	            e.printStackTrace();
	        }

	    }
	    
	    @FXML
	    void getRegles(javafx.event.ActionEvent event) {
	    	try {
	            Parent root = FXMLLoader.load(getClass().getResource("/application/ReglesJeu.fxml"));
	            Scene scene = new Scene(root);
	            scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
	            Stage stage =new Stage();
	            stage.setResizable(false);
	            
	          stage.setTitle("Tic-Tac-Toe");
	          stage.setScene(scene);
	          stage.show();
	        } catch(Exception e) {
	        	System.out.append("erreur ");
	            e.printStackTrace();
	        }

	    }
	    
	   
	    //////////////////////////////////
	    //une methode pour gerer l'animation du cicle 
	    public void setRotate(Circle c,boolean reverse,int angle,int duration) {
	    	RotateTransition rt=new RotateTransition(Duration.seconds(duration),c);
	    	rt.setAutoReverse(reverse);
	    	rt.setByAngle(angle);
	    	rt.setDelay(Duration.seconds(0));
	    	rt.setRate(3);
	    	rt.setCycleCount(18);
	    	rt.play();
}

		@Override
		public void initialize(URL arg0, ResourceBundle arg1) {
			// TODO Auto-generated method stub
			setRotate(c1, true, 360, 10);
	    	setRotate(c2, true, 180, 18);
	    	setRotate(c3, true, 145, 24);
			
		}

	   
}
