package application;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Task;
import javafx.concurrent.Worker;
import javafx.event.ActionEvent;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.HashMap;

import com.sun.glass.events.MouseEvent;

import ai.MultiLayerPerceptron;
import ai.SigmoidalTransferFunction;
import ai.Test;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TextArea;


public class MainViewController {
	
	@FXML
    private Button bouton1;

    @FXML
    private TextArea textArea;
    
    @FXML
    private ProgressBar bar;
    
    @FXML
    private CheckBox F;
    

    @FXML
    private CheckBox D;
	
	
	
	Task<Void> task = new Task<Void>() {
		
		
   	    //int size = 9;
        @Override protected Void call() throws Exception{
        	double epochs = 10000000 ;
    		//int[] layers = new int[]{ size,10, size };
    		double error = 0.0 ;
    		//MultiLayerPerceptron net = new MultiLayerPerceptron(layers, 0.1, new SigmoidalTransferFunction());
    		//HashMap<Integer, Coup> mapTrain = Test2.loadCoupsFromFile("./resources/train_dev_test/train.txt");
    		System.out.append("salut");
    		
        	String Fichier ="./resources/tran_dev_test/config.txt";
	   		BufferedReader br =new BufferedReader(new InputStreamReader(new FileInputStream(new File(Fichier))));
	   		System.out.append("je suis la");
	   		
	   		String Facile = br.readLine();
	   		String Difficile = br.readLine();
	   		
	   		String [] TabFacile=Facile.split(":");
	   		String [] TabDifficile=Difficile.split(":");
	   		
	   		int hF= Integer.parseInt(TabFacile[1]);
	   		int hD= Integer.parseInt(TabDifficile[1]);
	   		
	   		double lrF = Integer.parseInt(TabFacile[2]);
	   		double lrD = Integer.parseInt(TabDifficile[2]);
	   		
	   		int lF= Integer.parseInt(TabFacile[3]);
	   		int lD= Integer.parseInt(TabDifficile[3]);
	   		
	   		String modelFacile = "mlp_"+hF+"_"+lrF+"_"+lF+".srl" ;
	   		String modelDifficile = "mlp_"+hD+"_"+lrD+"_"+lD+".srl";
	   		
        	
    		if(F.isSelected()) {
	   			System.out.append("la checkbox facile");
	   			if(new File(modelFacile).exists()) {
	       			MultiLayerPerceptron net = MultiLayerPerceptron.load("models/"+modelFacile);
	       		}
	       		else {
	       			int [] layers=new int[lF+2];
	       			layers[0]=9;
	       			for(int i=0;i<lF;i++) {
	       				layers[i+1]=hF;
	       				
	       			}
	       			layers[layers.length-1]=9;
	       			MultiLayerPerceptron ne = new MultiLayerPerceptron(layers, lrF, new SigmoidalTransferFunction());
	       			HashMap<Integer, Coup> mapTrain = Test2.loadCoupsFromFile("./resources/train_dev_test/train.txt");
	       			int u = 0;
		 			for(int i = 0; i < epochs; i++){
		 				Coup c = null ;
		 				while ( c == null )
		 					c = mapTrain.get((int)(Math.round(Math.random() * mapTrain.size())));

		 				error += ne.backPropagate(c.in, c.out);
		 				float pourcentage;
		 				if ( i % 10000 == 0 ) {
		 					pourcentage = (float) ((i/epochs)*100);
		 					updateMessage("Error at step "+i+" is "+ (error/(double)i)+"---> Pourcentage :"+ pourcentage);
		 				}
		 				updateProgress(i, epochs);
		 				u=i;
		 			}
		 			updateProgress(1, 1);
		 			updateMessage("Error at step "+u+" is "+ (error/(double)u)+"---> Pourcentage : 100");
		 			return null;
	       		}
	   			
	   		}
    		
    		else if(D.isSelected()) {
	   			if(new File(modelDifficile).exists()) {
	       			MultiLayerPerceptron net = MultiLayerPerceptron.load("models/"+modelDifficile);
	       		}
	       		else {
	       			int [] layers=new int[lF+2];
	       			layers[0]=9;
	       			for(int i=0;i<lF;i++) {
	       				layers[i+1]=hF;
	       				}
	       			layers[layers.length-1]=9;
	       			MultiLayerPerceptron n = new MultiLayerPerceptron(layers, lrD, new SigmoidalTransferFunction());
	       			HashMap<Integer, Coup> mapTrain = Test2.loadCoupsFromFile("./resources/train_dev_test/train.txt");
	       		 int u = 0;
	 			for(int i = 0; i < epochs; i++){
	 				Coup c = null ;
	 				while ( c == null )
	 					c = mapTrain.get((int)(Math.round(Math.random() * mapTrain.size())));

	 				error += n.backPropagate(c.in, c.out);
	 				float pourcentage;
	 				if ( i % 10000 == 0 ) {
	 					pourcentage = (float) ((i/epochs)*100);
	 					updateMessage("Error at step "+i+" is "+ (error/(double)i)+"---> Pourcentage :"+ pourcentage);
	 				}
	 				updateProgress(i, epochs);
	 				u=i;
	 			}
	 			updateProgress(1, 1);
	 			updateMessage("Error at step "+u+" is "+ (error/(double)u)+"---> Pourcentage : 100");
	 			return null;
	       		}
	   			
	   		}
    		return null;
        }
        
	};
        	
        	
 	     	
        	
       		
       	

		
		
		  
	  

    
    
    Thread t = new Thread(task);
    
    
   ////////////////////////////////////
    //un button pour lancer l'apprntissage du model choisi t nottament l'alimentation de la barre de progression au fur et � mesure
    //de l'avancement de l'apprentissage 
    @FXML
    void launch(ActionEvent event) {
    	
    	t.setDaemon(true);
    	t.start();
    	
    	bar.progressProperty().unbind();
    	textArea.cacheProperty().unbind();
    	//bar.progressProperty().bind(task.progressProperty());
    	
    	
    	task.messageProperty().addListener(new ChangeListener<String>() {
    		@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
    			textArea.setText(newValue);
				
			}
    	});
    	
    	task.progressProperty().addListener(new ChangeListener<Number>() {
			@Override
			public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
				bar.setProgress((double) newValue);				
			}
    		
    	});	
    	
    }
    
   
	@FXML
    void stop(ActionEvent event) {
    	
    }


}
