package application;


import java.net.URL;
import java.util.ResourceBundle;

import javafx.animation.RotateTransition;
import javafx.animation.TranslateTransition;
import javafx.beans.property.IntegerProperty;

import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;
import javafx.util.Duration;

/**
 *  La Classe jeuController qui permet de gerer l'interface de jeu H Vs H  

 */

public class jeuController implements Initializable{
	
	
		
	 //COMPOSANTS FXML POUR LA TABLE SCORES ENREGISTRES 
	    @FXML
	    private TableView<Score> Table;
	    @FXML
	    private TableColumn<Score, StringProperty> NomTable;

	    @FXML
	    private TableColumn<Score, IntegerProperty> ScoreTable;
	    
	  /////////////////////////////////////
	    @FXML
	    private Button retour;
	    
	    @FXML
	    private Button buttonQuitter;
	    
	    @FXML
	    private Button enregistrer;
	    
	    @FXML
	    private Button Recommencer;
	 
	    @FXML
	    private AnchorPane rootJeu;
	   
	    @FXML
	    private ImageView ImageVs;
	    
	    @FXML
	    private Label score1;

	    @FXML
	    private Label score2;
	    
	 
	    @FXML
	    private Label TourJoueur;
	
	 	@FXML
	 	private Label setNomJoueur1;
	

	    @FXML
	    private Label setNomJoueur2;
	    
	    @FXML
	    private Label gagnant;
	
        @FXML
	    private ImageView ImageJoueur1;

	    @FXML
	    private ImageView ImageJoueur2;
	    
	    ///////////////////////////////////////////////////////
	    
	    //IMAGEVIEW POUR CHAQUE CASE DANS LA MATRICE DE JEU 9 X 9 CASES
	    

	    @FXML
	    private javafx.scene.image.ImageView ImageView1;

	    @FXML
	    private javafx.scene.image.ImageView ImageView2;

	    @FXML
	    private javafx.scene.image.ImageView ImageView3;

	    @FXML
	    private javafx.scene.image.ImageView ImageView4;

	    @FXML
	    private javafx.scene.image.ImageView ImageView5;

	    @FXML
	    private javafx.scene.image.ImageView ImageView6;

	    @FXML
	    private javafx.scene.image.ImageView ImageView7;

	    @FXML
	    private javafx.scene.image.ImageView ImageView8;

	    @FXML
	    private javafx.scene.image.ImageView ImageView9;
	    
	    
	    /////////////////////////////////////////////
	  
	    
	    @FXML
	    private Label labelTransition;

	   ////////////////////////////////////////////////
	    //CHARGER LES IMAGES CROIX ET ROND 
	    
	    Image ButtonPressed=new Image("images/rondRouge.png");
	    Image ButtonReleased = new Image("images/croixverte.png");
	    Image ImageWin = new Image("images/win.png");
	    
	    //////////////////////////////////
	    // cases EST UN TABLEAU DE LA MATRICE DE JEU DE TAILLE 9
	    //ON INITIALISE LE COMPTEUR  (NbrClic) DE CLIC A ZERO
	    //ON INITIALISE LES SCORES DES DEUX JOUEURS A ZERO
	    int []cases=new int[9];
		int NbrClic=0;
		int scoreJoueur1 = 0;
		int scoreJoueur2 = 0;
	    
	    
	    //JOUEUR 1 VA JOUER AVEC DES RONDS
	    //JOUEUR 2 VA JOUER AVEC DES CROIX
		
		/////////////////////////////////////////
		
		/**
		 *  DANS CHAQUE EVENEMENT MOUSEPRSSD ASSOCIER A CHAQUE IMAGEVIEW VOICI CE QUI SE PASSE 
		 *  -ON INCRIMENTE LE NbrClic 
		 *  -ON APPELLE LA FONCTION JeuHvsH avec le NbrClic POUR SAVOIR A QUI LE TOUR
		 *  -UNE FOIS ON CONNAIT LE JOUEUR --> ON AJOUTE  1 DANS  cases[]
		 *  -ON CHARGE L'IMAGE APPROPRIE A CE JOUEUR
		 *  -ON AFFICHE LE TOUR DU JOUEUR PROCHAINE 
		 *  -ON FAIT APPEL A LA FONCTION TestGagnant EN PARAMETRE:
		 * 	  @param cases[] 
		 *    @param 1 ou 2 selon l joueur 

		 */

	    @FXML
	    public void MousePressed(javafx.scene.input.MouseEvent event) {
	    	NbrClic += 1;
	    	int ChoixJoueur=JeuHvsH(NbrClic);
	    	System.out.append("le numero du joueur Actuel est "+ChoixJoueur);
	    	if(ChoixJoueur == 1) {
	    		cases[0]=1;
		    	ImageView1.setImage(ButtonPressed);
		    	System.out.append("le button rond est place!!!!");
		    	String Joueur2= setNomJoueur2.getText();
		    	if(!Joueur2.equals("")) {
			    	TourJoueur.setText("C'est Votre Tour "+Joueur2);
			    	TourJoueur.setStyle("-fx-text-fill: white; -fx-font-size: 14px;");
			    	}
		    	TestGagnant(cases, 1);
	    		
	    	}
	    	else if(ChoixJoueur==2){//joueur2 qui a jouer donc on mis une croix
	    		cases[0]=2;
		    	ImageView1.setImage(ButtonReleased);
		    	System.out.append("le button croix est place!!!!");
		    	String Joueur1= setNomJoueur1.getText();
		    	if(!Joueur1.equals("")) {
			    	TourJoueur.setText("C'est Votre Tour "+Joueur1);
			    	TourJoueur.setStyle("-fx-text-fill: white; -fx-font-size: 14px;");
			    	}
		    	TestGagnant(cases, 2);
	    		
	    	}
	    	
	    	
	    }
	    
	    @FXML
	    public void MousePressed2(javafx.scene.input.MouseEvent event) {
	    	NbrClic += 1;
	    	int ChoixJoueur=JeuHvsH(NbrClic);
	    	System.out.append("le numero du joueur Actuel est "+ChoixJoueur);
	    	if(ChoixJoueur == 1) {
	    		cases[1]=1;
		    	ImageView2.setImage(ButtonPressed);
		    	System.out.append("le button rond est place!!!!");
		    	String Joueur2= setNomJoueur2.getText();
		    	if(!Joueur2.equals("")) {
			    	TourJoueur.setText("C'est Votre Tour "+Joueur2);
			    	TourJoueur.setStyle("-fx-text-fill: white; -fx-font-size: 14px;");
			    	}
		    	TestGagnant(cases, 1);
	    		
	    	}
	    	else if(ChoixJoueur==2){//joueur2 qui a jouer donc on mis une croix
	    		cases[1]=2;
		    	ImageView2.setImage(ButtonReleased);
		    	System.out.append("le button croix est place!!!!");
		    	String Joueur1= setNomJoueur1.getText();
		    	if(!Joueur1.equals("")) {
			    	TourJoueur.setText("C'est Votre Tour "+Joueur1);
			    	TourJoueur.setStyle("-fx-text-fill: white; -fx-font-size: 14px;");
			    	}
		    	TestGagnant(cases, 2);
	    		
	    	}
	    	
	    		
	    }
	   
	    @FXML
	    public void MousePressed3(javafx.scene.input.MouseEvent event) {
	    	NbrClic += 1;
	    	int ChoixJoueur=JeuHvsH(NbrClic);
	    	System.out.append("le numero du joueur Actuel est "+ChoixJoueur);
	    	if(ChoixJoueur == 1) {
	    		cases[2]=1;
		    	ImageView3.setImage(ButtonPressed);
		    	System.out.append("le button rond est place!!!!");
		    	String Joueur2= setNomJoueur2.getText();
		    	if(!Joueur2.equals("")) {
			    	TourJoueur.setText("C'est Votre Tour "+Joueur2);
			    	TourJoueur.setStyle("-fx-text-fill: white; -fx-font-size: 14px;");
			    	}
		    	TestGagnant(cases, 1);
	    		
	    	}
	    	else if(ChoixJoueur==2){//joueur2 qui a jouer donc on mis une croix
	    		cases[2]=2;
		    	ImageView3.setImage(ButtonReleased);
		    	System.out.append("le button croix est place!!!!");
		    	String Joueur1= setNomJoueur1.getText();
		    	if(!Joueur1.equals("")) {
			    	TourJoueur.setText("C'est Votre Tour "+Joueur1);
			    	TourJoueur.setStyle("-fx-text-fill: white; -fx-font-size: 14px;");
			    	}
		    	
		    	TestGagnant(cases, 2);
	    		
	    	}
	    	
	    }
	    
	    @FXML
	    public void MousePressed4(javafx.scene.input.MouseEvent event) {
	    	NbrClic += 1;
	    	int ChoixJoueur=JeuHvsH(NbrClic);
	    	System.out.append("le numero du joueur Actuel est "+ChoixJoueur);
	    	if(ChoixJoueur == 1) {
	    		cases[3]=1;
		    	ImageView4.setImage(ButtonPressed);
		    	System.out.append("le button rond est place!!!!");
		    	String Joueur2= setNomJoueur2.getText();
		    	if(!Joueur2.equals("")) {
			    	TourJoueur.setText("C'est Votre Tour "+Joueur2);
			    	TourJoueur.setStyle("-fx-text-fill: white; -fx-font-size: 14px;");
			    	}
		    	TestGagnant(cases, 1);
	    		
	    	}
	    	else if(ChoixJoueur==2){//joueur2 qui a jouer donc on mis une croix
	    		cases[3]=2;
		    	ImageView4.setImage(ButtonReleased);
		    	System.out.append("le button croix est place!!!!");
		    	String Joueur1= setNomJoueur1.getText();
		    	if(!Joueur1.equals("")) {
			    	TourJoueur.setText("C'est Votre Tour "+Joueur1); 
			    	TourJoueur.setStyle("-fx-text-fill: white; -fx-font-size: 14px;");
			    	}
		    	TestGagnant(cases, 2);
	    		
	    	}
	    }
	    
	    @FXML
	    public void MousePressed5(javafx.scene.input.MouseEvent event) {
	    	NbrClic += 1;
	    	int ChoixJoueur=JeuHvsH(NbrClic);
	    	System.out.append("le numero du joueur Actuel est "+ChoixJoueur);
	    	if(ChoixJoueur == 1) {
	    		cases[4]=1;
		    	ImageView5.setImage(ButtonPressed);
		    	System.out.append("le button rond est place!!!!");
		    	String Joueur2= setNomJoueur2.getText();
		    	if(!Joueur2.equals("")) {
			    	TourJoueur.setText("C'est Votre Tour "+Joueur2);
			    	TourJoueur.setStyle("-fx-text-fill: white; -fx-font-size: 14px;");
			    	}
		    	TestGagnant(cases, 1);
	    		
	    	}
	    	else if(ChoixJoueur==2){//joueur2 qui a jouer donc on mis une croix
	    		cases[4]=2;
		    	ImageView5.setImage(ButtonReleased);
		    	System.out.append("le button croix est place!!!!");
		    	String Joueur1= setNomJoueur1.getText();
		    	if(!Joueur1.equals("")) {
			    	TourJoueur.setText("C'est Votre Tour "+Joueur1);
			    	TourJoueur.setStyle("-fx-text-fill: white; -fx-font-size: 14px;");
			    	}
		    	TestGagnant(cases, 2);
	    		
	    	}
	    }
	    
	    @FXML
	    public void MousePressed6(javafx.scene.input.MouseEvent event) {
	    	
	    	NbrClic += 1;
	    	int ChoixJoueur=JeuHvsH(NbrClic);
	    	System.out.append("le numero du joueur Actuel est "+ChoixJoueur);
	    	if(ChoixJoueur == 1) {
	    		cases[5]=1;
		    	ImageView6.setImage(ButtonPressed);
		    	System.out.append("le button rond est place!!!!");
		    	String Joueur2= setNomJoueur2.getText();
		    	if(!Joueur2.equals("")) {
			    	TourJoueur.setText("C'est Votre Tour "+Joueur2);
			    	TourJoueur.setStyle("-fx-text-fill: white; -fx-font-size: 14px;");
			    	}
		    	TestGagnant(cases, 1);
	    		
	    	}
	    	else if(ChoixJoueur==2){//joueur2 qui a jouer donc on mis une croix
	    		cases[5]=2;
		    	ImageView6.setImage(ButtonReleased);
		    	System.out.append("le button croix est place!!!!");
		    	String Joueur1= setNomJoueur1.getText();
		    	if(!Joueur1.equals("")) {
			    	TourJoueur.setText("C'est Votre Tour "+Joueur1);
			    	TourJoueur.setStyle("-fx-text-fill: white; -fx-font-size: 14px;");
			    	}
		    	TestGagnant(cases, 2);
	    		
	    	}
	    }
	    
	    @FXML
	    public void MousePressed7(javafx.scene.input.MouseEvent event) {
	    	NbrClic += 1;
	    	int ChoixJoueur=JeuHvsH(NbrClic);
	    	System.out.append("le numero du joueur Actuel est "+ChoixJoueur);
	    	if(ChoixJoueur == 1) {
	    		cases[6]=1;
		    	ImageView7.setImage(ButtonPressed);
		    	System.out.append("le button rond est place!!!!");
		    	String Joueur2= setNomJoueur2.getText();
		    	if(!Joueur2.equals("")) {
			    	TourJoueur.setText("C'est Votre Tour "+Joueur2);
			    	TourJoueur.setStyle("-fx-text-fill: white; -fx-font-size: 14px;");
			    	}
		    	TestGagnant(cases, 1);
	    		
	    	}
	    	else if(ChoixJoueur==2){//joueur2 qui a jouer donc on mis une croix
	    		cases[6]=2;
		    	ImageView7.setImage(ButtonReleased);
		    	System.out.append("le button croix est place!!!!");
		    	String Joueur1= setNomJoueur1.getText();
		    	if(!Joueur1.equals("")) {
			    	TourJoueur.setText("C'est Votre Tour "+Joueur1);
			    	TourJoueur.setStyle("-fx-text-fill: white; -fx-font-size: 14px;");
			    	}
		    	TestGagnant(cases, 2);
	    		
	    	}
	    }
	    
	    @FXML
	    public void MousePressed8(javafx.scene.input.MouseEvent event) {
	    	NbrClic += 1;
	    	int ChoixJoueur=JeuHvsH(NbrClic);
	    	System.out.append("le numero du joueur Actuel est "+ChoixJoueur);
	    	if(ChoixJoueur == 1) {
	    		cases[7]=1;
		    	ImageView8.setImage(ButtonPressed);
		    	System.out.append("le button rond est place!!!!");
		    	String Joueur2= setNomJoueur2.getText();
		    	if(!Joueur2.equals("")) {
			    	TourJoueur.setText("C'est Votre Tour "+Joueur2);
			    	TourJoueur.setStyle("-fx-text-fill: white; -fx-font-size: 14px;");
			    	}
		    	TestGagnant(cases, 1);
	    		
	    	}
	    	else if(ChoixJoueur==2){//joueur2 qui a jouer donc on mis une croix
	    		cases[7]=2;
		    	ImageView8.setImage(ButtonReleased);
		    	System.out.append("le button croix est place!!!!");
		    	String Joueur1= setNomJoueur1.getText();
		    	if(!Joueur1.equals("")) {
			    	TourJoueur.setText("C'est Votre Tour"+Joueur1);
			    	TourJoueur.setStyle("-fx-text-fill: white; -fx-font-size: 14px;");
			    	}
		    	TestGagnant(cases, 2);
	    		
	    	}
	    }
	    
	    @FXML
	    public void MousePressed9(javafx.scene.input.MouseEvent event) {
	    	NbrClic += 1;
	    	int ChoixJoueur=JeuHvsH(NbrClic);
	    	System.out.append("le numero du joueur Actuel est "+ChoixJoueur);
	    	if(ChoixJoueur == 1) {
	    		cases[8]=1;
		    	ImageView9.setImage(ButtonPressed);
		    	System.out.append("le button rond est place!!!!");
		    	String Joueur2= setNomJoueur2.getText();
		    	if(!Joueur2.equals("")) {
		    	TourJoueur.setText("C'est Votre Tour "+Joueur2);
		    	TourJoueur.setStyle("-fx-text-fill: white; -fx-font-size: 14px;");
		    	}
		    	TestGagnant(cases, 1);
	    		
	    	}
	    	else if(ChoixJoueur==2){//joueur2 qui a jouer donc on mis une croix
	    		cases[8]=2;
		    	ImageView9.setImage(ButtonReleased);
		    	System.out.append("le button croix est place!!!!");
		    	String Joueur1= setNomJoueur1.getText();
		    	if(!Joueur1.equals("")) {
		    	TourJoueur.setText("C'est Votre Tour "+Joueur1);
		    	TourJoueur.setStyle("-fx-text-fill: white; -fx-font-size: 14px;");
		    	}
		    	TestGagnant(cases, 2);
	    		
	    	}
	    	
	    	
	    }
	    
	    /////////////////////////////////////////////////
	    /**
	     * Fonction pour relancer la partie selon le nombre de clic fournie par les deux joueur
	     * Remise des Compteurs A zero NbrClic --> 0 & mettre toute les valeurs du tableau cases a zero 
	    */
	   
	    @FXML
	    void RelancerPartie(ActionEvent event) {
	    	//rendre le ImageView non cliquable
	    		ImageView1.setDisable(false);
	    		ImageView2.setDisable(false);
	    		ImageView3.setDisable(false);
	    		ImageView4.setDisable(false);
	    		ImageView5.setDisable(false);
	    		ImageView6.setDisable(false);
	    		ImageView7.setDisable(false);
	    		ImageView8.setDisable(false);
	    		ImageView9.setDisable(false);
			
	    	// enlever les Images des imageView
	    		ImageView1.setImage(null);
	    		ImageView2.setImage(null);
	    		ImageView3.setImage(null);
	    		ImageView4.setImage(null);
	    		ImageView4.setImage(null);
	    		ImageView5.setImage(null);
	    		ImageView6.setImage(null);
	    		ImageView7.setImage(null);
	    		ImageView8.setImage(null);
	    		ImageView9.setImage(null);
	    		NbrClic = 0 ; //remise du compteur de Clic A zero pour une nouvelle partie 
	    		for(int i=0;i<9;i++) { //remise du tableau de cases a zero 
	    			cases[i]=0;
	    		}
	    		gagnant.setText(null);
	    		TourJoueur.setText(null);
	    		
	    		
	    	

	    }
	    
	  
         //////////////////////////////////////////
	   
	    //ENREGISTRER LE SCORE DES DEUX JOUEURS DANS LA TABLE SCORES ENTREGISTRES
	    
        @FXML
	    void EnregistrerResultat(ActionEvent event) {
        	
            NomTable.setCellValueFactory(new PropertyValueFactory<Score,  StringProperty>("NomTable"));
	        ScoreTable.setCellValueFactory(new PropertyValueFactory<Score, IntegerProperty>("ScoreTable"));
	        final ObservableList<Score> data = FXCollections.observableArrayList();
	    	data.add(new Score("essaid",10));
	    	data.add(new Score("Pedro",15));
	    	data.add(new Score("Pepe",60));
	    	data.add(new Score(setNomJoueur1.getText(),Integer.valueOf(score1.getText())));
	    	data.add(new Score(setNomJoueur2.getText(),Integer.valueOf(score2.getText())));
	    	
	       Table.getItems().addAll(data);
	         
	    	}
      
	
        //////////////////////////////////////////
	    //Une fonction qui permet de determiner le tour d'un joueur 
	   //En Param C'est le nombre de clic 
	    
	    public int  JeuHvsH(int nbrClic) {
	    	 int  joueur = 0;
	    	
	    	if(nbrClic <= 9) {
	    		if(nbrClic % 2 == 0) {
	    			joueur=1;
	    			
	    		}
	    		else {
	    			joueur=2;
	    			
	    		}
	    		
	    	}
	    	
	    	return joueur;
	    	
	    }
	    
	    ////////////////////////////////////////////////////////
	    
	    
	    
	    public boolean GagnantHorizontal(int cases[],int joueur) {
	    	
	        if(cases[0] == joueur && cases[1] ==joueur && cases[2] == joueur  ) {
	        	ImageView1.setImage(ImageWin);
	        	ImageView2.setImage(ImageWin);
	        	ImageView3.setImage(ImageWin);
	    		return true;
	    	}
	        if(cases[3] == joueur && cases[4] ==joueur && cases[5] == joueur  ) {
	        	ImageView4.setImage(ImageWin);
	        	ImageView5.setImage(ImageWin);
	        	ImageView6.setImage(ImageWin);
	    		return true;
	    	}
	        if(cases[6] == joueur && cases[7] ==joueur && cases[8] == joueur  ) {
	        	ImageView7.setImage(ImageWin);
	        	ImageView8.setImage(ImageWin);
	        	ImageView9.setImage(ImageWin);
	    		return true;
	    	}
	        else {
	        return false ;
	        }
	    }
	    
	    ///////////////////////////////////////////////////
	    //TEST GAGNANT POUR LES CASES VERTICALS 
	    //AFFICHER L'IMAGE GAGNANT DANS LES CASES CORRESPONDANTES 
	    
	    
        public boolean GagnantVertical (int cases[],int joueur) {
	    	
	        if(cases[0] == joueur && cases[3] ==joueur && cases[6] == joueur  ) {
	        	ImageView1.setImage(ImageWin);
	        	ImageView4.setImage(ImageWin);
	        	ImageView7.setImage(ImageWin);
	    		return true;
	    	}
	        if(cases[1] == joueur && cases[4] ==joueur && cases[7] == joueur  ) {
	        	ImageView2.setImage(ImageWin);
	        	ImageView5.setImage(ImageWin);
	        	ImageView8.setImage(ImageWin);
	    		return true;
	    	}
	        if(cases[2] == joueur && cases[5] ==joueur && cases[8] == joueur  ) {
	        	ImageView3.setImage(ImageWin);
	        	ImageView6.setImage(ImageWin);
	        	ImageView9.setImage(ImageWin);
	    		return true;
	    	}
	        
	        else {
	        return false ;
	        }
	    }
        
        
        /////////////////////////////////////////////
        //TEST GAGNANT POUR LES CASES EN DIAGONALS
	    //AFFICHER L'IMAGE GAGNANT DANS LES CASES CORRESPONDANTES 
        
        public boolean GagnantDiagonal (int cases[],int joueur) {
	    	
	        if(cases[0] == joueur && cases[4] ==joueur && cases[8] == joueur  ) {
	        	ImageView1.setImage(ImageWin);
	        	ImageView5.setImage(ImageWin);
	        	ImageView9.setImage(ImageWin);
	    		return true;
	    	}
	        if(cases[2] == joueur && cases[4] ==joueur && cases[6] == joueur  ) {
	        	ImageView3.setImage(ImageWin);
	        	ImageView5.setImage(ImageWin);
	        	ImageView7.setImage(ImageWin);
	    		return true;
	    	}
	       
	       else {
	        return false ;
	        }
	    }
        
        ////////////////////////////////////////////////////////
        /**
         *  TestGagnant une fonction qui permet de verifier si il existe un gagnant ou pas 
         *  si vrai ---> le rest des cases ne sont plus cliquable -->on incrimente le score du joueur gagnant -->fin partie 
         *  @param cases : un tableau avec des cases jouer par le joueur 
         *  @param joueur : la personne qui a jouer le coup 

         */

         public void TestGagnant(int cases[],int joueur) {
        	
        	if(GagnantDiagonal(cases, joueur) || GagnantHorizontal(cases, joueur)|| GagnantVertical(cases, joueur) ) {
        		ImageView1.setDisable(true);
				ImageView2.setDisable(true);
				ImageView3.setDisable(true);
				ImageView4.setDisable(true);
				ImageView5.setDisable(true);
				ImageView6.setDisable(true);
				ImageView7.setDisable(true);
				ImageView8.setDisable(true);
				ImageView9.setDisable(true);
				
				if(joueur == 1) { //si le joueur 1 est gagnant
            					    System.out.println(" on a un gagnant ");
            						gagnant.setText("Felicitation! Le joueur 1 est Gagnant ");
            					    gagnant.setStyle("-fx-text-fill: white; -fx-font-size: 15px;");
            						
            					    scoreJoueur1 +=1;
            						System.out.println("le resultat du joueur 1 est  "+scoreJoueur1);
            						score1.setText(String.valueOf(scoreJoueur1));
            						
            						
            	}
            	else if(joueur == 2) {
            		System.out.println(" on a un gagnant ");
    				gagnant.setText("Felicitation! Le joueur 2 est Gagnant");
    				gagnant.setStyle("-fx-text-fill: white; -fx-font-size: 15px;");
    				
    				scoreJoueur2 +=1;
    				System.out.println("le resultat du joueur 2 est  "+scoreJoueur2);
					score2.setText(String.valueOf(scoreJoueur2));
            	}
            		
            }
            		
           else {
        	   		System.out.println(" pas de gagnant ");
            		gagnant.setText("A vous de jouer! ");
            	}
            	for(int i=0;i<cases.length;i++) {
            		System.out.println("la valeur"+i+" de la cases :"+cases[i]);
            	}
            	
        	
       }
         
         
    
	    
	 
	    /////////////////////////////////////////////////////////
        //Action associer au button Retour au menu 
	    
	    @FXML
	    void UploadView(ActionEvent event) {
	    	try {
	            Parent root = FXMLLoader.load(getClass().getResource("/application/Accueil.fxml"));
	            Scene scene = new Scene(root);
	            scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
	            Stage stage =new Stage();
	            stage.setResizable(false);
	            
	          stage.setTitle("Accueil");
	          stage.setScene(scene);
	          stage.show();
	        } catch(Exception e) {
	        	System.out.append("erreur ");
	            e.printStackTrace();
	        }

	    }
	    /////////////////////////////////////////////////////////
        //Action pour quitter la partie
	    
	    @FXML
	    void Quitter(ActionEvent event) {
	    	Stage stage = (Stage) buttonQuitter.getScene().getWindow();
		     stage.close();

	    }
	    
	    public void setMoved(Circle c,int duration,int cyrcleCount,int delay) {
	    	RotateTransition rt=new RotateTransition(Duration.seconds(duration),c);
	    	
	    	rt.setDelay(Duration.seconds(0));
	    	rt.setRate(3);
	    	rt.setCycleCount(18);
	    	
	    	rt.play();
	    	
	    }
	    ///////////////////////
	    //au lancement de la fen�tre On lance le Text bienvenu en utilisant le processus de transition "TranslateTransition"
	    

		@Override
		public void initialize(URL arg0, ResourceBundle arg1) {
			TranslateTransition Translate = new TranslateTransition(Duration.seconds(5), labelTransition);
			Translate.setToX(labelTransition.getLayoutX() +rootJeu.getPrefWidth());
			Translate.play();
		
		}
		
		//////////////////////////////
		//recuperer les noms des joueurs envoyer par la fenetre NomJoueur
		public void myfunction(String text1,String text2) {
			setNomJoueur1.setText(text1);
			setNomJoueur2.setText(text2);
		}
	    

    


}
